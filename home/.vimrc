source $VIMRUNTIME/defaults.vim
source $VIMRUNTIME/colors/desert.vim

let mapleader=" "

set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number
set relativenumber

set clipboard=unnamedplus

" Ortografia
command Or :setlocal spell! spelllang=it,pl,en
command Orhelp :help spell
command Oroff :set nospell

" TEXT
autocmd FileType text setlocal textwidth=78

" YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" MARKDOWN
autocmd FileType markdown inoremap ** ****<esc>hi
autocmd FileType markdown inoremap * **<esc>i
autocmd FileType markdown vnoremap ** di****<esc>hPll
autocmd FileType markdown vnoremap * di**<esc>Pl

autocmd FileType markdown inoremap mdimg ![TITLE](https://)<esc>?[<CR>a
autocmd FileType markdown inoremap mdlink [TITLE](https://)<esc>?[<CR>a

